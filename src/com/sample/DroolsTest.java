package com.sample;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

import com.sample.ItemCity.City;
import com.sample.ItemCity.Type;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.*;
import org.drools.event.rule.DebugAgendaEventListener;
import org.drools.event.rule.DebugWorkingMemoryEventListener;
import org.drools.io.ResourceFactory;
import org.drools.logger.KnowledgeRuntimeLogger;
import org.drools.logger.KnowledgeRuntimeLoggerFactory;
import org.drools.runtime.StatefulKnowledgeSession;

import static com.sample.Measurer.cpu;
import static com.sample.Measurer.memoryUsage;
import static com.sample.Measurer.processorUsage;

/* This is a sample class to launch a rule. */

public class DroolsTest {
    public static void main(String[] args) {
        try {

            // load up the knowledge base
            KnowledgeBase kbase = readKnowledgeBase();
            StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();

            /*
            Inicjalizacja danych w regule "initial" w pliku .drl
             */

//            ItemCity item1 = new ItemCity();
//            item1.setPurchaseCity(City.PUNE);
//            item1.setTypeofItem(Type.MEDICINES);
//            item1.setSellPrice(new BigDecimal(10));
//            ksession.insert(item1);
//
//            ItemCity item2 = new ItemCity();
//            item2.setPurchaseCity(City.PUNE);
//            item2.setTypeofItem(Type.GROCERIES);
//            item2.setSellPrice(new BigDecimal(10));
//            ksession.insert(item2);
//
//            ItemCity item3 = new ItemCity();
//            item3.setPurchaseCity(City.NAGPUR);
//            item3.setTypeofItem(Type.MEDICINES);
//            item3.setSellPrice(new BigDecimal(10));
//            ksession.insert(item3);
//
//            ItemCity item4 = new ItemCity();
//            item4.setPurchaseCity(City.NAGPUR);
//            item4.setTypeofItem(Type.GROCERIES);
//            item4.setSellPrice(new BigDecimal(10));
//            ksession.insert(item4);

            // setup the debug listeners
//            ksession.addEventListener( new DebugAgendaEventListener() );
//            ksession.addEventListener( new DebugWorkingMemoryEventListener() );



            // setup the audit logging

//            KnowledgeRuntimeLogger logger = KnowledgeRuntimeLoggerFactory.newFileLogger(ksession, "log/helloworld");

            new Thread(new Measurer()).start();
            while (cpu == -1) {
                System.out.println("Waiting for measuring thread...");
                Thread.sleep(3000);
            }
            BufferedWriter bw = new BufferedWriter(new FileWriter("results.txt",true));
            Date start = new Date();
            ksession.fireAllRules();
            Date end = new Date();

            Measurer.ifContinue = false;

            Long [] mem = memoryUsage.toArray(new Long[memoryUsage.size()]);
            long minMem = mem[0];
            long maxMem = mem[0];
            for (long m : mem) {
                if (m < minMem) minMem = m;
                if (m > maxMem) maxMem = m;
            }

            Double [] proc = processorUsage.toArray(new Double[processorUsage.size()]);
            double minProc = proc[0];
            double maxProc = proc[0];
            for (double p : proc) {
                if (p < minProc) minProc = p;
                if (p > maxProc) maxProc = p;
            }
            try {
                bw.write("Mem diff: ");
                bw.write(String.valueOf( (maxMem - minMem) / 1024L));
                bw.write(" KB , Processor diff: ");
                bw.write(String.valueOf(maxProc-minProc));
                bw.write(" % , Time diff: ");
                bw.write(String.valueOf(end.getTime() - start.getTime()));
                bw.write(" ms \n-------\n");
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

//            System.out.println(item1.getPurchaseCity().toString() + " "
//                    + item1.getLocalTax().intValue());
//
//            System.out.println(item2.getPurchaseCity().toString() + " "
//                    + item2.getLocalTax().intValue());
//
//            System.out.println(item3.getPurchaseCity().toString() + " "
//                    + item3.getLocalTax().intValue());
//
//            System.out.println(item4.getPurchaseCity().toString() + " "
//                    + item4.getLocalTax().intValue());
            System.out.println(ksession.getObjects().size());
            System.out.println("---------------------------------");
            for (Object o : ksession.getObjects()) {
                System.out.println(o.toString());
            }

            //ksession.getObjects();

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private static KnowledgeBase readKnowledgeBase() throws Exception {

        System.out.println(System.getProperty("java.version"));
        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();

//        kbuilder.add(ResourceFactory.newClassPathResource("Pune.drl"), ResourceType.DRL);
//        kbuilder.add(ResourceFactory.newClassPathResource("Nagpur.drl"), ResourceType.DRL);
//        kbuilder.add(ResourceFactory.newFileResource(new File("Pune.drl")),ResourceType.DRL);
//        kbuilder.add(ResourceFactory.newFileResource(new File("Nagpur.drl")),ResourceType.DRL);
        kbuilder.add(ResourceFactory.newFileResource(new File("psc-zatrudnienie-ocena_kandydata.drl")),ResourceType.DRL);

                KnowledgeBuilderErrors errors = kbuilder.getErrors();

        if (errors.size() > 0) {
            for (KnowledgeBuilderError error: errors) {
                System.err.println(error);
            }
            throw new IllegalArgumentException("Could not parse knowledge.");
        }

        KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
        kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());

        return kbase;
    }
}
